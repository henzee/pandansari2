<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>Warung PandanSari</title>
<!--

Template 2081 Solution

http://www.tooplate.com/view/2081-solution

-->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

		<!-- animate -->
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- bootstrap -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- font-awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- google font -->
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Sigmar+One" rel="stylesheet">
		<!-- custom -->
		<link rel="stylesheet" href="css/style.css">
		<!--slick theme-->
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/slick-theme.css">
		<!-- DataTables -->
	  <link rel="stylesheet" href="css/dataTables.bootstrap.css">

	</head>
	<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

		<!-- start navigation -->
		<div class="navbar navbar-fixed-top navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand"><h1 id="logo"><span>W</span>arung <span>P</span>andansari</h1></a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" id="nav-menu">
						<li><a href="#" class="nav-active">PANDANSARI</a></li>
						<li><a href="#">LAYANAN</a></li>
						<li><a href="menu.php">MENU</a></li>
						<li><a href="#">FASILITAS</a></li>
						<li><a href="contact.php">HUBUNGI</a></li>
						<li><a href="galeri.php">GALERI</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end navigation -->

		<div class="main-content">
      <div class="row" id="service-page" style="margin-top:7em;width:85%;margin-left:auto;margin-right:auto;">
        <div class="col-md-6" style="padding-left:0;">
          <div class="row" style="">
						<div class="col-md-3" style="font-family: 'Sigmar One', cursive;color:#e74c3c;text-align:center">
							<h5 style="text-decoration:underline;">layanan</h5>
							<h3 style="font-family: 'Sigmar One', cursive;font-size:3em;margin-top:0">01</h3>
						</div>
						<div class="col-md-9" style="padding-top:0.3em;padding-left:0">
							<p><span style="color:#e74c3c;">Lorem Ipsum</span> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
						</div>
					</div>
        </div>
				<div class="col-md-6" style="padding-left:0;">
          <div class="row" style="">
						<div class="col-md-3" style="font-family: 'Sigmar One', cursive;color:#e74c3c;text-align:center">
							<h5 style="text-decoration:underline;">layanan</h5>
							<h3 style="font-family: 'Sigmar One', cursive;font-size:3em;margin-top:0">02</h3>
						</div>
						<div class="col-md-9" style="padding-top:0.3em;padding-left:0">
							<p><span style="color:#e74c3c;">Lorem Ipsum</span> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
						</div>
					</div>
        </div>
				<div class="col-md-6" style="padding-left:0;">
          <div class="row" style="">
						<div class="col-md-3" style="font-family: 'Sigmar One', cursive;color:#e74c3c;text-align:center">
							<h5 style="text-decoration:underline;">layanan</h5>
							<h3 style="font-family: 'Sigmar One', cursive;font-size:3em;margin-top:0">03</h3>
						</div>
						<div class="col-md-9" style="padding-top:0.3em;padding-left:0">
							<p><span style="color:#e74c3c;">Lorem Ipsum</span> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
						</div>
					</div>
        </div>
				<div class="col-md-6" style="padding-left:0;">
          <div class="row" style="">
						<div class="col-md-3" style="font-family: 'Sigmar One', cursive;color:#e74c3c;text-align:center">
							<h5 style="text-decoration:underline;">layanan</h5>
							<h3 style="font-family: 'Sigmar One', cursive;font-size:3em;margin-top:0">04</h3>
						</div>
						<div class="col-md-9" style="padding-top:0.3em;padding-left:0">
							<p><span style="color:#e74c3c;">Lorem Ipsum</span> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
						</div>
					</div>
        </div>
				<div class="col-md-6" style="padding-left:0;">
          <div class="row" style="">
						<div class="col-md-3" style="font-family: 'Sigmar One', cursive;color:#e74c3c;text-align:center">
							<h5 style="text-decoration:underline;">layanan</h5>
							<h3 style="font-family: 'Sigmar One', cursive;font-size:3em;margin-top:0">05</h3>
						</div>
						<div class="col-md-9" style="padding-top:0.3em;padding-left:0">
							<p><span style="color:#e74c3c;">Lorem Ipsum</span> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
						</div>
					</div>
        </div>
      </div>
		</div>
		<!--end main-content-->

		<div style="text-align:center;margin-bottom:1em;">
			<p>
				<h3>Tetap terhubung</h3>
			</p>
		</div>

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row social-icon wow fadeInUp" data-wow-delay="0.9s">
					<div class="col-md-12 col-sm-12">

					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-facebook fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-twitter fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-instagram fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-pinterest fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-google fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-youtube fa-2x"></a>
					</div>
				</div>
				<div class="copyright">
					<p>Copyright &copy; <span>Warung Pandansari</span></p>
				</div>
			</div>
		</footer>
		<!-- end footer -->

		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<!-- bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- isotope -->
		<script src="js/isotope.js"></script>
		<!-- images loaded -->
   	<script src="js/imagesloaded.min.js"></script>
   		<!-- wow -->
		<script src="js/wow.min.js"></script>
		<!-- jquery flexslider -->
		<script src="js/jquery.flexslider.js"></script>
		<!-- custom -->
		<script src="js/custom.js"></script>
	</body>
</html>
