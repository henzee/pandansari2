<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>Warung PandanSari</title>
<!--

Template 2081 Solution

http://www.tooplate.com/view/2081-solution

-->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

		<!-- animate -->
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- bootstrap -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- font-awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- google font -->
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Sigmar+One" rel="stylesheet">
		<!-- custom -->
		<link rel="stylesheet" href="css/style.css">
		<!--slick theme-->
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/slick-theme.css">
		<!-- DataTables -->
	  <link rel="stylesheet" href="css/dataTables.bootstrap.css">

	</head>
	<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

		<!-- start navigation -->
		<div class="navbar navbar-fixed-top navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand"><h1 id="logo"><span>W</span>arung <span>P</span>andansari</h1></a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right" id="nav-menu">
						<li><a href="#" class="nav-active">PANDANSARI</a></li>
						<li><a href="#">LAYANAN</a></li>
						<li><a href="menu.php">MENU</a></li>
						<li><a href="#">FASILITAS</a></li>
						<li><a href="contact.php">HUBUNGI</a></li>
						<li><a href="galeri.php">GALERI</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end navigation -->

		<div class="main-content">

			<!-- start home -->
			<section id="home" class="text-center">
			  <div class="templatemo_headerimage">
			    <div class="flexslider">
			      <ul class="slides">
			        <li>
			        	<img src="images/slider/1.jpg" alt="Slide 1">
			        	<div class="slider-caption">
						    <div class="templatemo_homewrapper">
						      <h2 class="wow fadeInDown" data-wow-delay="2000">
										Cita Rasa Indonesia
									</h2>
						    </div>
					  	</div>
			        </li>
			        <li>
			        	<img src="images/slider/2.jpg" alt="Slide 2">
			        	<div class="slider-caption">
						    <div class="templatemo_homewrapper">
						      <h2 class="wow fadeInDown" data-wow-delay="2000">
										Menggugah Selera
									</h2>
						    </div>
					  	</div>
			        </li>
			      </ul>
			    </div>
			  </div>
			</section>
			<!-- end home -->

			<!-- start service -->
			<div id="service">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="row media">
								<div class="col-md-6 col-sm-6 wow fadeIn">
									<img src="images/makanan.jpg"/>
								</div>
								<div class="col-md-6 col-sm-6">
									<h3><span>M</span>akanan</h3>
									<p>Kami menjual berbagai makanan. Dengan beberapa variasi,  digoreng, dibakar.</p>
									<div class="btn-style"><a href="#">Lihat Menu</a></div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="row media">
								<div class="col-md-6 col-sm-6 wow fadeIn">
									<img src="images/soda.jpg"/>
								</div>
								<div class="col-md-6 col-sm-6">
									<h3><span>M</span>inuman</h3>
									<p>Kami menjual berbagai makanan. Dengan beberapa variasi,  digoreng, dibakar.</p>
									<div class="btn-style"><a href="#">Lihat Menu</a></div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="row media">
								<div class="col-md-6 col-sm-6 wow fadeIn">
									<img src="images/bakmi.jpg"/>
								</div>
								<div class="col-md-6 col-sm-6">
									<h3><span>P</span>aket <span>P</span>rasmanan</h3>
									<p>Kami menjual berbagai makanan. Dengan beberapa variasi,  digoreng, dibakar.</p>
									<div class="btn-style"><a href="#">Lihat Menu</a></div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="row media">
								<div class="col-md-6 col-sm-6 wow fadeIn">
									<img src="images/sambal.jpg"/>
								</div>
								<div class="col-md-6 col-sm-6">
									<h3><span>P</span>aket <span>N</span>asi <span>K</span>otak</h3>
									<p>Kami menjual berbagai makanan. Dengan beberapa variasi,  digoreng, dibakar.</p>
									<div class="btn-style"><a href="#">Lihat Menu</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end service -->

			<!-- start divider -->
			<div id="divider">
				<div class="container">
					<div class="row">
						<div class="col-md-1 col-sm-1"></div>
						<div class="col-md-8 col-sm-8">
							<h2 class="wow bounce">Kami memberikan layanan <strong>terbaik</strong></h2>
							<p class="wow fadeInUp" data-wow-delay="0.9s">Nulla ultricies bibendum augue et molestie. Suspendisse pellentesque mollis imperdiet. Quisque sodales laoreet tincidunt. Phasellus ut mi orci. Vivamus id odio ac justo tincidunt placerat. Nulla facilisi. Vivamus et dolor urna. Sed vestibulum urna justo, nec malesuada urna aliquet et.</p>
							<div class="btn-style"><a href="#">Segera Kunjungi</a></div>
						</div>
						<div class="col-md-2 col-sm-2"></div>
					</div>
				</div>
			</div>
			<!-- end divider -->
		</div>
		<!--end main-content-->

		<div style="text-align:center;margin-bottom:1em;">
			<p>
				<h3>Tetap terhubung</h3>
			</p>
		</div>

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row social-icon wow fadeInUp" data-wow-delay="0.9s">
					<div class="col-md-12 col-sm-12">

					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-facebook fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-twitter fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-instagram fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-pinterest fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-google fa-2x"></a>
					</div>
					<div class="col-md-2 col-sm-2">
						<a href="#" class="fa fa-youtube fa-2x"></a>
					</div>
				</div>
				<div class="copyright">
					<p>Copyright &copy; <span>Warung Pandansari</span></p>
				</div>
			</div>
		</footer>
		<!-- end footer -->

		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<!-- bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- isotope -->
		<script src="js/isotope.js"></script>
		<!-- images loaded -->
   	<script src="js/imagesloaded.min.js"></script>
   		<!-- wow -->
		<script src="js/wow.min.js"></script>
		<!-- jquery flexslider -->
		<script src="js/jquery.flexslider.js"></script>
		<!-- custom -->
		<script src="js/custom.js"></script>
	</body>
</html>
